#   Réseaux de neurones à impulsions et Optimisation de paramètres sur Grille de Calcul

##  Présentation générale

Sujet pas encore affecté. Ce sujet est susceptible de se prolonger par 
deux mois de contrat ingénieur cet été et par une thèse en Octobre 2019.

### Résumé

Le but du stage est double : d’une part une partie théorique sur les 
architectures de réseaux de neurones et leur paramétrage, et une partie 
appliquée sur la simulation de ces architectures sur la plateforme 
Grid'5000 pour l'optimisation de paramétres. Parmi les questions 
ouvertes qui pourront être traitées, on trouve : la description à haut 
niveau du paramétrage de ces architectures, de l'évaluation de leur 
capacités d'apprentissage pour permettre une optimisation de ces 
architectures quand la fonction de cout est particulièrement lourde. 
L’objectif est de permettre dans le futur le "tuning" le plus 
automatique possible de réseaux de neurones à impulsions.

### Mots-clés

Réseau de neurones, Optimisation combinatoire, Calcul haute performance
### Encadrement

Équipe(s) : Emeraude

Encadrant(s) :

-   Pierre Boulet
-   Philippe Devienne
-   Nouredine Melab
-   El Ghazali Talbi

[Contacter les 
encadrants](mailto:Philippe.Devienne@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : IRCICA/CRIStAL, Parc de la haute borne, Villeneuve d’Ascq

##  Présentation détaillée

### Pré-requis

L'environnement de travail : Linux, Scala (+ Akka)

### Contexte

Ce projet de Master s’intéresse aux nouvelles architectures 
d’ordinateurs ayant le potentiel de prendre le relais des architectures 
à base de transistors pour poursuivre l’amélioration des performances de 
nos ordinateurs après la fin de la loi de Moore.  Le modèle étudié est 
ici celui d’un réseau de neurones et synapses artificiels développés par 
nos partenaires des nano-électroniciens de l'IEMN de Lille

### Problématique

Pour évaluer les caractéristiques de ces nouvelles architectures, nous 
avons déjà développé un simulateur, N2S3  en Scala. Le cœur de ce 
simulateur est un modèle événementiel temporisé. Il intègre les modèles 
physiques des  neurones et synapses développés à l’IEMN de Lille par 
l’équipe d'Alain Cappy.

Le but de ce simulateur est d’explorer différents types d’architectures 
de neurones et de synapses et nous guider vers la réalisation matérielle 
de capteurs ou d’accélérateurs intelligents, dotés de capacité 
d’apprentissage automatique.

Pour faciliter l’utilisation de N2S3 et mieux comprendre les 
simulations, nous avons besoin d’étendre ses modèles d’exécution vers 
des modèles distribués


### Travail à effectuer

La première partie consistera en une étude des architectures de réseaux 
de neurones et leur paramétrage. Ceci pourra se faire via le simulateur 
N2S3 qui a été mis au point dans l'équipe (en Scala et Akka).
La deuxième partie consistera en la description à haut niveau du 
paramétrage de ces architectures, de l'évaluation de leur capacités 
d'apprentissage pour permettre une optimisation de ces architectures 
quand la fonction de cout est particulièrement lourde. L’objectif est de 
permettre dans le futur le "tuning" le plus automatique possible de 
réseaux de neurones à impulsions.
Une implémentation distribuée du simulation de ces architectures sur la 
plateforme Grid'5000 pour l'optimisation de paramètres.

Toutes ces recherches sont soumises aux contraintes dues à l’objectif 
scientifique de l’équipe qui est de préparer la réalisation d’un chip 
hardware, un accélérateur neuromorphique. Il faut donc que les 
architectures simulées soient réalistes d’un point de vue technologique. 
Elles s’appuient donc sur des modèles de nano-électronique contraints 
par les possibilités technologiques.


### Bibliographie

L'apprentissage profond : une révolution en intelligence artificielle : 
https://www.college-de-france.fr/site/yann-lecun/inaugural-lecture-2016-02-04-18h00.htm
N2S3 : https://sourcesup.renater.fr/wiki/n2s3/start
Akka : https://akka.io/
Grid'5000 : https://www.cristal.univ-lille.fr/?page=plateforme&id=1
