#  HPC pour BLINEIDE

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

Le but de ce stage est d'améliorer la partie
calcul Haute Performance de la bibliothèque BLINEIDE.

### Mots-clés

HPC, OpenCL, algorithmique numérique,
intégration numérique, GPU, SIMD, programmation par tâches

### Encadrement

Équipe(s) : CFHP (Calcul Formel et Haute Performance)

Encadrant(s) :

- François Boulier (Francois.Boulier@univ-lille.fr)
- Pierre Fortin (Pierre.Fortin@lip6.fr)
- François Lemaire (Francois.Lemaire@univ-lille.fr)

[Contacter les encadrants](mailto:Francois.Boulier@univ-lille.fr,Pierre.Fortin@lip6.fr,Francois.Lemaire@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Laboratoire CRIStAL, Université de Lille


##  Présentation détaillée

### Pré-requis

Programmation en C, goût pour le HPC
(programmation et algorithmique) et pour le calcul 
scientifique, forte motivation, rigueur et capacités d'analyse.

Aucune compétence n'est requise en algorithmique numérique.

Des compétences en HPC sont bienvenues, mais non nécessaires. 

Ce stage permet d'acquérir une première expérience significative en
HPC et en algorithmique numérique, et offre des possibilités de
poursuite en thèse.


### Contexte

L'équipe CFHP est spécialiste du traitement des équations
différentielles ordinaires (ODE en anglais). Dans le cadre d'une
application à la biologie qui vise à mieux comprendre et modéliser
l'interaction Neurone-Astrocyte qui joue un rôle important lors des
accidents vasculaires cérébraux, l'équipe s'intéresse aux équations
intégro-différentielles (IDE) [CASC18]. Ces IDE ont la
particularité de faire intervenir des termes intégraux, et permettent
de modéliser des phénomènes complexes dépendant de l'état passé du
système, comme par exemple celui de la toxicité d'un produit qui
diminuerait dans le temps.  La bibliothèque BLINEIDE [BLINEIDE]
(développée dans l'équipe) permet d'intégrer numériquement les IDE en
s'appuyant sur les méthodes d'Euler, Runge-Kutta, et de Simpson
[CNUM]

### Problématique

Cette bibliothèque vient d'être déployée sur CPU multi-coeur via du
parallélisme de tâches, et nous souhaitons désormais l'accélérer
davantage en exploitant d'autres niveaux de parallélisme et d'autres
architectures de calcul haute performance (high performance
computing - HPC).

### Travail à effectuer

Le premier objectif est de concevoir un code portable sur diverses
architectures HPC : CPU multi-coeur, GPU discret, GPU intégré, voire
FPGA à plus long terme.  On utilisera pour cela le standard OpenCL
[OpenCL].  L'enjeu sera ici de concevoir une plateforme
logicielle adaptée à ces différentes architectures, et de pouvoir
intégrer automatiquement des IDE fournies par l'utilisateur.

On s'intéressera ensuite à l'optimisation des performances sur ces
diverses architectures, avec des enjeux spécifiques sur chacune.  Sur
CPU multi-coeur on visera une vectorisation effective des noyaux de
calcul sur les unités SIMD des coeurs CPU : ces unités SIMD (AVX2 ou
AVX-512) peuvent en effet offrir des gains de performance allant
jusqu'à 16x.  Sur GPU, on s'intéressera à l'impact sur les
performances des transferts de données via le bus PCI : on pourra
ainsi comparer les GPU discrets (puissants mais accessibles via le bus
PCI) aux GPU intégrés (moins puissants mais ne nécessitant pas ces
transferts PCI).

Enfin, on pourra étudier une exécution hybride CPU-GPU. Pour cela, on
pourra étendre le parallélisme par tâche existant dans BLINEIDE aux
architectures hybrides 
à l'aide de systèmes de tâches avancés comme
StarPU [StarPU]. Ceci pourra aussi permettre d'optimiser les
performances sur GPU discrets en exploitant tous les niveaux de
parallélisme offerts par BLINEIDE. 

Le matériel adéquat sera mis à disposition : un serveur HPC avec deux
processeurs à 18 coeurs chacun (deux CPU Intel Xeon E5-2695 v4, avec
72 threads matériels), ainsi que des serveurs intégrant des GPU haut
de gamme pour le HPC (GPU NVIDIA K40 et P100, avec respectivement 2880
et 3584 coeurs GPU), et des GPU intégrés AMD et Intel.



### Bibliographie


[CASC18]
  François Boulier, Hélène Castel, Nathalie Corson, Valentina Lanza, François
  Lemaire, Adrien Poteaux, Alban Quadrat, and Nathalie Verdière. 
  Symbolic-Numeric Methods for Nonlinear Integro-Differential Modeling.
  Computer Algebra and Scientific Computing (CASC) 2018. 

[BLINEIDE]
  François Boulier.
  http://cristal.univ-lille.fr/~boulier/BLINEIDE

[CNUM]
  François Boulier.
  Cours de calcul numérique.
http://cristal.univ-lille.fr/~boulier/polycopies/CNUM2A5/support.pdf
      
[OpenCL]
  Standard OpenCL.
  https://www.khronos.org/opencl/

[StarPU]
  StarPU, a task programming library for hybrid architectures.
  http://starpu.gforge.inria.fr/
