#  Réconciliation des réseaux sociaux

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

Ce stage de fin d’études cible la problématique de la réconciliation des données dans les réseaux sociaux. En particulier, nous ciblons l’étude des mécanismes d’authentification sociaux comme vecteur de connection des données issues de plusieurs réseaux sociaux. En effet, les réseaux sociaux, comme Facebook, mettent à disposition un moyen d’authentification unique pour des sites tiers et permettent à ces derniers d’importer certaines données personnelles. En créant un tel lien sémantique entre Facebook et des sites tiers, nous pensons qu’il devient possible de construire un graphe de connaissances impliquant plusieurs réseaux sociaux.

### Mots-clés

social network, REST, database

### Encadrement

Équipe(s) : Spirals (CRIStAL / Inria)

Encadrant(s) :

- Pierre Bourhis, Chargé de recherche
- Romain Rouvoy, Professeur

[Contacter les encadrants](mailto:romain.rouvoy@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille - Nord Europe, 40 avenue Halley, 59650 Villeneuve d’Ascq.

##  Présentation détaillée

### Pré-requis

- Bonne connaissance des bases de données (schéma de données)
- Bonne connaissance des applications REST (protocole HTTP, format JSON)

### Contexte

Ce stage de fin d’étude s’inscrit dans le cadre du projet Momentum «_Gérer vos données sans fuite d'information_» financé par le CNRS et porté par Pierre Bourhis. Ce projet vise à renforcer le respect de l’intimité des usagers sur Internet en identifiant des fuites potentielles d’informations privées et en proposant des contre-mesures efficaces pour mieux maîtriser la nature des données personnelles qui peuvent être accédées par des tiers non-autorisés. Ce projet revêt donc un enjeu sociétal pour l’ensemble de la population.

### Problématique

Nombre d’applications web—y compris de réseaux sociaux spécialisés—proposent désormais aux utilisateurs de s’authentifier en utilisant leur compte Facebook (ou Google ou autre) et d’importer certaines données pour alimenter un profil utilisateur, par exemple. Cependant, ces données peuvent être indirectement être exposées par des sites tiers et ainsi contribuer à créer des liens implicites entre les différents profils d’une même personne, révélant ainsi des informations potentiellement intimes.

### Travail à effectuer
À une échelle locale, nous proposons notamment d’adopter une approche logique pour modéliser les données des réseaux sociaux et les liens potentiels entre ceux-ci.

À une échelle globale, nous proposons d’identifier les stratégies de contrôle d’accès aux données selon le profil d’un utilisateur (extérieur, authentifié, etc.).

