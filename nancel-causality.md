# Interagir avec la chronologie dans les applications de dessin

##  Présentation générale

Sujet pas encore affecté.

### Résumé

Ce projet va consister à appliquer la méthodologie dite de "Research Through Design" (RtD) [1,2] pour évaluer le potentiel de nouvelles interactions avec les historiques de commandes [3] dans les logiciels de dessin. Permettre des modifications partielles d'événements ou de commandes effectuées dans le passé (proche ou lointain) d'un document offre de nombreuses opportunités pour l'exploration d'alternatives de création et d'édition dans des domaines d'application riches comme le dessin. Le stagiaire utilisera d'abord des méthodes qualitatives pour identifier le potentiel de ces techniques pour des applications de dessin. En accord avec les méthodes de RtD, le stagiaire va concevoir des prototypes itératifs et les évaluer avec des utilisateurs novices et professionnels.

### Mots-clés

Interaction Homme-Machine, Historiques de commande, Research through design

### Encadrement

Équipe : [Loki](http://loki.lille.inria.fr/)

Encadrant :

-   [Mathieu Nancel](http://mathieu.nancel.net)

[Contacter l'encadrant](mailto:mathieu@nancel.net).

Localisation : Inria Lille – Nord Europe, bâtiment B

## Présentation détaillée

## Pré-requis

Le candidat idéal est étudiant en master en informatique, et montre un grand intérêt pour la recherche en Interaction Homme-Machine.

Le candidat doit avoir de l'expérience dans les méthodes de conception (*design*), de préférence dans la méthodologie RtD, et être capable de conduire des *interviews* et des ateliers. De fortes connaissances techniques sont un plus, et seront requises pour poursuivre ce travail dans une thèse.

## Description

Les historiques de commandes et leurs fonctionnalités ont peu évolué depuis leur conception initiale il y a plus de 30 ans, et les commandes classiques "annuler-refaire". Ils permettent de naviguer et de corriger des erreurs récentes dans la chronologie d'un document, mais ces fonctionnalités restent limitées et peuvent causer de graves pertes d'information [3]. De nombreuses améliorations de ces commandes ont été proposées par des chercheurs en Interaction Homme-Machine (IHM) : *timelines* parallèles, annuler des actions qui ne sont pas les dernières dans la liste, ou à partir de leur position dans le document, etc. Cependant, ces techniques ont été conçues et évaluées indépendamment, et sont en général incompatibles entre elles – parfois même avec les fonctions de base annuler-refaire ! En conséquence, elles ne sont quasiment jamais implémentées dans des logiciels réels.

Un modèle unifié d’historiques d’interaction a été proposé [3] qui permet la navigation, la réutilisation et la modification de toute commande passée, mais aussi de leurs paramètres et de leur *input* utilisateur. Le travail présenté dans [3] est un modèle conceptuel qui décrit quelles informations conserver, et comment les organiser entre elles afin que toute fonctionnalité portant sur l'interaction avec les historiques de commandes puisse être implémentée. L'objectif de ce modèle est d'être appliqué et adapté à des domaines spécifiques, comme l'édition de texte riche, de vidéo, ou de dessin.

Il faut maintenant définir les fonctionnalités les plus intéressantes et les mieux adaptées aux utilisateurs, ainsi que les meilleures façons de rendre ces fonctionnalités compréhensibles et interactives.

Le stagiaire appliquera les méthodes de RtD pour explorer les réactions, les préférences et les idées des utilisateurs face aux possibilités décrites dans [3], afin de guider la conception de prototypes interactifs fonctionnels et d'ouvrir la voie à une thèse portant sur les historiques de commandes avancés. Le stage va consister à mener des *interviews* et des sessions de *brainstorming* et de prototypage avec des utilisateurs de logiciels de dessin. Cela inclura la conception itérative de maquettes et de vidéos de démonstration qui seront présentées aux participants, dont les retours permettront de réaliser des prototypes interactifs fonctionnels.

## Bibliographie

[1] Zimmerman, J., Forlizzi, J., & Evenson, S. (2007). Research through design as a method for interaction design research in HCI. ACM CHI. https://doi.org/10.1145/1240624.1240704 

[2] Gaver, W. (2012). What should we expect from research through design? ACM CHI. http://dx.doi.org/10.1145/2207676.2208538 

[3] Nancel, M. & Cockburn, A. (2014). Causality – A Conceptual Model of Interaction History. ACM CHI. http://dx.doi.org/10.1145/2556288.2556990 