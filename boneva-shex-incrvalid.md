#   Validation incrémentale de contraintes pour RDF

##  Présentation générale

Sujet pas encore affecté.
<!-- Sujet affecté à : ⟨...⟩. -->

### Résumé

RDF est le format recommandé par pour stocker et publier les données sur le Web des données et le Web sémantique. Il existe aujourd'hui une grande quantité de données au format RDF, aussi bien des données libres que des données au sein des entreprises.
Shape Expression Schemas (ShEx) est un langage récent permettant de garantir la qualité des données RDF en définissant et en vérifiant des contraintes sur les données.
Lors de ce stage, l'étudiant·e devra développer des algorithmes efficaces pour la validation incrémentale de contraintes exprimées en ShEx, c'est à dire, mettre à jour la validité des contraintes lorsque les données sont mises à jour, en limitant las calculs au strict nécessaire. 

### Mots-clés

Qualité des données, Algorithmes, Web sémantique, Données graphe et RDF, Shape Expression Schemas

### Encadrement

Équipe(s) : Links

Encadrant(s) : 

-   Iovka Boneva (iovka.boneva@univ-lille.fr)

[Contacter les encadrants](mailto:iovka.boneva@univ-lille.fr?subject=Stage%20de%20recherche).

Localisation : Inria Lille – Nord Europe, bâtiment B


##  Présentation détaillée

### Pré-requis

- bon niveau en algorithmique et en structures de données
- bon niveau de compréhension en anglais
- (optionnel) connaissance des technologies de base du Web sémantique (RDF, SPARQL, OWL)

### Contexte

RDF est le format recommandé par le W3C pour stocker et publier les données sur le Web des données et le Web sémantique. Il en résulte une immense collection de données (datasets) interconnectées, comprenant des données libres publiées par les institutions et par diverses organisations (par ex. Wikidata géré par la fondation Wikimedia, Uniprot base de donnée collaborative sur les protéines), mais aussi des données propriétaires utilisées au sein des entreprises (par ex. dans des bases de connaissances pour la recherche biomédicale, des bases de faits au sein des journaux et agences de presse).

RDF est intentionnellement un langage non contraint, par opposition à une base de données relationnelle où les données sont stockées suivant un schéma rigide. Ceci permet à ce que la même collection de données puisse être utilisée dans plusieurs applications, chaque application définissant ses propres contraintes sur la portion de données appropriée.

Le langage Shape Expressions Schemas (ShEx) permet de définir des contraintes spécifiques pour une application pour les données RDF. Le langage est récent, développé de manière collaborative au sein d'un Community group au W3C. L'équipe Links est fortement impliquée dans la définition du langage, et tout particulièrement dans la définition des algorithmes de validation de ShEx.

### Problématique

ShEx vient avec différents algorithmes qui permettent de tester si des données RDF satisfont les contraintes ShEx. Nous voulons étendre ces algorithmes pour la validation incrémentale. C'est à dire, mettre à jour la validité des contraintes lorsque les données sont mises à jour. 

### Travail à effectuer

Après une formation au langage ShEx et aux algorithmes existants, la ou le stagiaire devra:
- identifier de quelle manière la mise à jour des données peut influencer leur validité par rapport à des contraintes ShEx
- proposer un algorithme de validation incrémentale qui effectue uniquement les calculs de validation rendus nécessaires par la mise à jour des données
- implémenter l'algorithme en java et l'intégrer au validateur ShEx existant développé dans l'équipe et/ou tester l'efficacité de l'algorithme sur des données réelles

### Bibliographie

Shape Expressions Language 2.0. http://shex.io/shex-semantics/. E. Prud'hommeaux, I.Boneva, J. Labra Gayo, G. Kellogg.

Semantics and Validation of Shapes Schemas for RDF. In ISWC2017 - 16th. International semantic web conference. I. Boneva, J. Labra Gayo and E. Prud'hommeaux.

RDF : https://www.w3.org/TR/2014/REC-rdf11-concepts-20140225/Overview.html

Linked Data. http://linkeddata.org/. Contains references to public linked datasets.


